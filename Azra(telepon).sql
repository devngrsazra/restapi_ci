/*
 Navicat Premium Data Transfer

 Source Server         : sqlserver
 Source Server Type    : SQL Server
 Source Server Version : 10504000
 Source Host           : FIRZAK-PC\SQLEXPRESS:1433
 Source Catalog        : Azra
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 10504000
 File Encoding         : 65001

 Date: 08/05/2019 10:09:14
*/


-- ----------------------------
-- Table structure for telepon
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[telepon]') AND type IN ('U'))
	DROP TABLE [dbo].[telepon]
GO

CREATE TABLE [dbo].[telepon] (
  [id] int  NOT NULL,
  [nama] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [nomor] varchar(13) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[telepon] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of telepon
-- ----------------------------
INSERT INTO [dbo].[telepon] VALUES (N'1', N'Firzak', N'08577686522')
GO

INSERT INTO [dbo].[telepon] VALUES (N'2', N'Faris,Arsyad,Vian,Alvin', N'12345')
GO

INSERT INTO [dbo].[telepon] VALUES (N'3', N'Beta', N'9856325432')
GO

INSERT INTO [dbo].[telepon] VALUES (N'7', N'Alpha', N'0869565687')
GO


-- ----------------------------
-- Primary Key structure for table telepon
-- ----------------------------
ALTER TABLE [dbo].[telepon] ADD CONSTRAINT [PK__kontak__3213E83F03317E3D] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

